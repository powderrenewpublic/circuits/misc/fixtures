$fn = 32;

mount_dims = [200, 200];
connector_bounds = [7, 8];

base_th = 5;
wall_th = 5;
mount_th = base_th + 30;

mhole_d = 3 + 0.2;

//z_offset = ((25-24.25) + (25-24.27) + (50-49.37) + (50-49.32))/4;
z_offset = 0; // for printer 2

//echo(z_offset);

//cube([5, 5, 10]);

//translate([10, 10, 0])
//cube([5, 5, 20]);

//cube([10, 10, 50+z_offset]);

//translate([20, 20, 0])
//cube([10, 10, 25+z_offset]);

// m3
nut_d = 8;
nut_th = 4;
m3_head_d = 5.68+1;

//ghole_spacing = 10;
//ghole_d = 3 + 0.2;
ghole_spacing = 25;
ghole_d = 6.5;
rail_edge_th = 5;
rail_min_l = 25 + rail_edge_th*2;

//sma_center_h = 31.5; // old height
con_center_h = 50 + z_offset;

m3_d = 3.1;
m3_clear_d = 3.4;
m2_clear_d = 2.4;
m2_nut_flat_hole = 3.88 + 0.4;

eps = 0.1;

board_dims_qpd = [20, 30];
mhole_offset_qpd = [5, 10];
//jig_base(board_dims_qpd, mhole_offset_qpd);

board_dims_npt = [23, 35];
mhole_offset_npt = [12.5/2, 25/2];
//jig_base(board_dims_npt, mhole_offset_npt);

clamp_s = 10;
clamp_gap = 1;

vna_connector_d = 21.8 + 1;
vna_support_l = 20;
vna_support_w = vna_connector_d + wall_th*2;

//translate([0, 0, 100])
//linear_extrude(1)
//import(file="NPTB00004A/gerbers/NPTB00004A-F_Cu.dxf");
//import(file="NPTB00004A/gerbers/NPTB00004A-Edge_Cuts.dxf");

//ecal_4_support();
module ecal_4_support() {
    tol = 3;

    //bounds = [56.47, 132.80, 31.32]; // z axis was wrong, I'm not sure how I messed up the measurement
    bounds = [56.47, 132.80, 48.10];
    hole_offset = [34.55/2, 108.66/2];
    hole_d = 7.10 + 2.5;

    con_offset_z = con_center_h - bounds[2]/2;

    render()
    difference() {
        translate([0, 0, con_offset_z/2])
        cube([bounds[0], bounds[1], con_offset_z], center=true);

        mirror_copy([1, 0])
        mirror_copy([0, 1])
        translate(hole_offset)
        cylinder(d=hole_d, h=con_offset_z);
    }
}

//rotate([180, 0, 90]) power_sensor_support(clamp=true);
//power_sensor_support(clamp=false);
module power_sensor_support(clamp=false) {
    tol = 0.25;

    bounds = [39.31, 93.90, 30.28];

    con_offset_z = con_center_h - bounds[2]/2;

    if(clamp) {

        // clamp fix, made tabs too big on the support, but this is faster to reprint
        render()
        difference() {
            union() {
                clamp(bounds[0]+tol, bounds[2]/2-1);

                mirror_copy([1, 0, 0])
                translate([(bounds[0]+tol)/2+wall_th, -10/2, 0])
                cube([10+wall_th, 10, wall_th]);
            }

            mirror_copy([1, 0, 0])
            translate([(bounds[0]+tol)/2+wall_th + 10/2 + wall_th, 0, 0])
            cylinder(d=m3_clear_d, h=wall_th);
        }
    }
    else {
        translate([0, 0, con_offset_z/2])
        cube([bounds[0]+tol, bounds[1], con_offset_z], center=true);

        linear_extrude(con_offset_z + bounds[2]/2 - 1)
        difference() {
            square([bounds[0]+tol + wall_th*2, bounds[1]], center=true);
            square([bounds[0]+tol, bounds[1]], center=true);
        }

        mirror_copy([1, 0, 0])
        translate([bounds[0]/2+tol/2 + wall_th, -10/2, con_offset_z + bounds[2]/2-1 - wall_th])
        render()
        difference() {
            cube([10+wall_th, 10, wall_th]);
        
            translate([10/2 + wall_th, 10/2])
            cylinder(d=m3_clear_d, h=wall_th);
        }
    }
}

//attenuator_support();
module attenuator_support() {
    // C8N2508-10
    // C8N2508-20

    tol = 0.4;

    att_hole_edge_spacing_w = (35.01 + 35.07 + 35.06)/3;
    att_hole_edge_spacing_l = (37.00 + 37.19 + 37.06 + 36.97)/4;
    att_hole_d = (2.70 + 2.73)/2;
    att_head_d = 5.67;
    att_head_depth = 10;
    att_hole_extra = 0.5;

    att_hole_spacing_w = att_hole_edge_spacing_w + att_hole_d;
    att_hole_spacing_l = att_hole_edge_spacing_l + att_hole_d;

    att_base_w = 56.95 + tol;
    att_base_l = att_hole_edge_spacing_l*3;

    att_con_h = ((6.38 + 4.84 + 15.75/2) + (6.35 + 5.02 + 15.81/2))/2;
    att_con_offset_z = con_center_h - att_con_h;

    label_gap = 40;

    mirror_copy([1, 0, 0])
    translate([att_base_w/2, 0, 0])
    render()
    difference() {
        translate([0, -att_base_l/2, 0])
        cube([5, att_base_l, att_con_offset_z + 10]);

        translate([0, -label_gap/2, 0])
        cube([5, label_gap, att_con_offset_z + 10]);
    }

    render()
    difference() {
        translate([0, 0, att_con_offset_z/2])
        cube([att_base_w, att_base_l, att_con_offset_z], center=true);

        for(hole_x = [-att_hole_spacing_w/2, att_hole_spacing_w/2]) {
            for(hole_y = [-att_hole_spacing_l, 0, att_hole_spacing_l]) {
                translate([hole_x, hole_y, 0]) {
                    linear_extrude(att_con_offset_z)
                    square(att_hole_d+att_hole_extra, center=true);

                    linear_extrude(att_con_offset_z-att_head_depth)
                    square(att_head_d+att_hole_extra, center=true);

                    translate([0, 0, att_con_offset_z-att_head_depth])
                    linear_extrude(0.5)
                    square([att_hole_d+att_hole_extra, att_head_d+att_hole_extra], center=true);
                }
            }
        }
    }

    mirror_copy([1, 0, 0])
    ghole_rails(2, att_base_l, att_base_w/2);
}

//pa_support();
module pa_support() {
    pa_hole_edge_spacing_l = 64.15;
    pa_hole_edge_spacing_w = 204.5;
    pa_base_side_w = 24.71 + 1;
    //pa_base_side_l = 223;

    pa_hole_d = 5.02;
    pa_nut_d = 9 + 1;
    pa_nut_flat = 7.88 + 0.5;
    pa_nut_th = 4.5 + 1;
    pa_nut_depth = 5;

    pa_n_h_1 = 12.60 + 11.70/2;
    pa_n_h_2 = 12.63 + 11.53/2;
    pa_n_h = (pa_n_h_1 + pa_n_h_2)/2;

    pa_sma_h_1 = 15.09 + 6.26/2;
    pa_sma_h_2 = 15.14 + 6.25/2;
    pa_sma_h = (pa_sma_h_1 + pa_sma_h_2)/2;

    pa_base_h_offset = (9.39 + 9.40)/2;
    pa_con_h = min(pa_n_h, pa_sma_h) + pa_base_h_offset;

    pa_hole_spacing_l = pa_hole_edge_spacing_l + pa_hole_d;
    pa_hole_spacing_w = pa_hole_edge_spacing_w + pa_hole_d;

    echo(pa_hole_spacing_w);

    pa_base_side_l = pa_hole_spacing_l*2 + pa_base_side_w + 30;

    con_offset_z = con_center_h - pa_con_h;
    con_offset_x = (72.75 - 87.63)/2;

    fast_simple = false;
    if(fast_simple) {
        !difference() {
            linear_extrude(con_offset_z)
            square(pa_base_side_w, center=true);

            linear_extrude(con_offset_z)
            square(pa_hole_d, center=true);

            translate([0, 0, con_offset_z-pa_nut_th-pa_nut_depth])
            linear_extrude(pa_nut_th)
            translate([-pa_nut_d/2, -pa_nut_flat/2])
            square([pa_base_side_w/2 + pa_nut_d/2, pa_nut_flat]);

            translate([0, 0, con_offset_z-pa_nut_depth])
            linear_extrude(0.5)
            square([pa_hole_d, pa_nut_flat], center=true);
        }
    }

    module pa_support_side() {
        render()
        difference() {
            translate([0, 0, con_offset_z/2])
            cube([pa_base_side_w, pa_base_side_l, con_offset_z], center=true);

            for(hole_offset = [-pa_hole_spacing_l, 0, pa_hole_spacing_l])
            translate([0, hole_offset, 0]) {
                linear_extrude(con_offset_z)
                square(pa_hole_d, center=true);

                translate([0, 0, con_offset_z-pa_nut_th-pa_nut_depth])
                linear_extrude(pa_nut_th)
                translate([-pa_nut_d/2, -pa_nut_flat/2])
                square([pa_base_side_w/2 + pa_nut_d/2, pa_nut_flat]);
                //cylinder(d=pa_hole_d, h=con_offset_z);

                translate([0, 0, con_offset_z-pa_nut_depth])
                linear_extrude(0.5)
                square([pa_hole_d, pa_nut_flat], center=true);
            }
        }

        mirror_copy([0, 1, 0])
        translate([vna_support_w/2 + pa_base_side_w/2, pa_base_side_l/2 - vna_support_l/2, 0])
        vna_connector_support(rails=false);
    }

    translate([-(pa_hole_spacing_w/2 + con_offset_x) + 20, 0, 0]) {
        translate([pa_hole_spacing_w/2 + con_offset_x, 0, 0])
        pa_support_side();

        ghole_rails(5, pa_base_side_l, pa_hole_spacing_w/2 + pa_base_side_w/2 + con_offset_x);
    }

    translate([pa_hole_spacing_w/2 - con_offset_x - 20, 0, 0]) {
        mirror([1, 0, 0])
        translate([pa_hole_spacing_w/2 - con_offset_x, 0, 0])
        pa_support_side();

        mirror([1, 0, 0])
        ghole_rails(6, pa_base_side_l, pa_hole_spacing_w/2 + pa_base_side_w/2 - con_offset_x);
    }
}

//c0208_20_support(flipped=false);
c0208_20_support(flipped=true);
module c0208_20_support(flipped=false) {
    bounds = [15.08, 45.41, 11.07];

    hole_d = 2.32;
    hole_pair_gap = 17.81 + hole_d;
    hole_pair_edge_offset = 0.95 + hole_d/2;
    hole_single_edge_offset = 11.89 + hole_d/2;

    sma_down = (8.48 + 8.53)/2;
    sma_up = (8.87 + 8.93)/2;
    sma_d = 6.19;
    sma_h1 = sma_down - sma_d/2;
    sma_h2 = bounds[2] - (sma_up - sma_d/2);

    sma_h = (sma_h1 + sma_h2)/2;
    sma_flipped_h = bounds[2] - sma_h;
    echo(sma_h, sma_flipped_h);

    sma_h_cond = flipped ? sma_flipped_h : sma_h;
    
    sma_offset_z = con_center_h - sma_h_cond;
    sma_offset_x = 3.37 + sma_d/2;

    translate([-sma_offset_x, 0, 0])
    render()
    difference() {
        translate([0, -bounds[1]/2, 0])
        cube([bounds[0], bounds[1], sma_offset_z]);

        translate([hole_single_edge_offset, 0, sma_offset_z])
        vert_cap_nut_hole(20, m2_clear_d, m2_nut_flat_hole, 3, 5);

        mirror_copy([0, 1, 0])
        translate([hole_pair_edge_offset, hole_pair_gap/2, sma_offset_z])
        mirror([1, 0, 0])
        vert_cap_nut_hole(20, m2_clear_d, m2_nut_flat_hole, 3, 5);
    }

    difference() {
        mirror_copy([1, 0, 0])
        ghole_rails(1, bounds[1]);

        if(flipped) {
            for(rot=[0, 180]) rotate(rot)
            translate([3*ghole_spacing/4, 0, base_th-0.5])
            rotate(90)
            linear_extrude(1)
            text("label down", halign="center", size=5);
        }
    }
}

//narda_22770_support(clamp=false);
//narda_22770_support(clamp=true);
module narda_22770_support(clamp=false) {
    tol = 0.25;
    bounds = [50.96+tol, 126.99+tol, 19.24];

    lip = 10;
    sma_offset_z = con_center_h - bounds[2]/2;
    sma_offset_x = bounds[0]/2 - 5.28 - 15.79/2;

    clamp_offset = bounds[0]/2 + clamp_s/2;

    wall_len = 10;
    wall_h = 10;

    if(clamp) {
        //translate([-sma_offset_x, 0, 70])
        rotate([90, 0, 0])
        render()
        difference() {
            translate([0, 0, (bounds[2]+5)/2-clamp_gap/2])
            cube([bounds[0]+clamp_s*2+10, clamp_s, bounds[2]+5-clamp_gap], center=true);

            translate([0, 0, bounds[2]/2-clamp_gap/2])
            cube([bounds[0], clamp_s, bounds[2]-clamp_gap], center=true);

            mirror_copy([1, 0, 0])
            union() {
                translate([bounds[0]/2+5, -clamp_s/2, 5])
                cube([clamp_s, clamp_s, bounds[2]]);

                translate([(bounds[0]+clamp_s)/2+5, 0, 0]) {
                    cylinder(d=m3_d, h=5);

                    //translate([0, 0, 5])
                    //cylinder(d=m3_head_d+1, h=100);
                }
            }
        }
    }
    else {
        translate([-sma_offset_x, 0, 0]) {
            linear_extrude(sma_offset_z) {
                center_hole(lip)
                square([bounds[0], bounds[1]], center=true);
            }

            translate([0, 0, sma_offset_z-5])
            linear_extrude(5) {
                mirror_copy([1, 0])
                translate([clamp_offset, 0])
                difference() {
                    translate([5/2, 0])
                    square([clamp_s+5, clamp_s], center=true);

                    translate([5, 0])
                    circle(d=m3_d);
                }
            }

            mirror_copy([0, 1, 0])
            translate([-bounds[0]/2, bounds[1]/2, sma_offset_z])
            hull() {
                cube([wall_len, 5, wall_h]);
                translate([0, 0, -wall_h])
                cube([wall_len, eps, wall_h]);
            }
        }

        mirror([1, 0, 0])
        ghole_rails(2, bounds[1]/2, bounds[0]/2+sma_offset_x);

        ghole_rails(1, bounds[1]/2, bounds[0]/2-sma_offset_x);
    }
}

//ct1060s3_support();
module ct1060s3_support() {
    tol = 0.25;

    bounds = [31.79+tol, 31.79+tol, 14.01];
    sma_top_offset = 5.14;
    sma_bottom_offset = 4.08;
    sma_offset = con_center_h - (bounds[2]-sma_top_offset + sma_bottom_offset)/2;

    rail_l = min(rail_min_l, bounds[1]);

    sma_cut_bounds = [10, 10];

    hole_d = 3.10;
    hole_offset = 1.8 + hole_d/2;

    lip = 6;
    lip_wall_h = 10;
    difference() {
    linear_extrude(sma_offset)
    difference() {
        center_hole(lip)
        square([bounds[0], bounds[1]], center=true);

        mirror_copy([1, 1, 0])
        translate([-bounds[0]/2+hole_offset, -bounds[1]/2+hole_offset])
        circle(d=hole_d+tol);
    }

        mirror_copy([1, 1, 0])
        translate([-bounds[0]/2+hole_offset, -bounds[1]/2+hole_offset, sma_offset - 5])
        cube([nut_d, nut_d, nut_th], center=true);
    }

    mirror_copy([1, 0])
    ghole_rails(1, rail_l, bounds[0]/2);

//    linear_extrude(sma_offset + lip_wall_h)
//    difference() {
//        center_hole(wall_th)
//        offset(delta=wall_th)
//        square([bounds[0], bounds[1]], center=true);
//
//        translate([bounds[0]/2, 0, 0])
//        square(sma_cut_bounds, center=true);
//
//        translate([0, bounds[1]/2, 0])
//        square(sma_cut_bounds, center=true);
//
//        translate([0, -bounds[1]/2, 0])
//        square(sma_cut_bounds, center=true);
//    }

//    mount_l = -2.5 * ghole_spacing;
//    mount_r = 2.5 * ghole_spacing;
//    difference() {
//        translate([mount_l-4, -bounds[1]/2-wall_th, 0])
//        cube([mount_r - mount_l + 4*2, bounds[1] + wall_th*2, base_th]);
//
//        hull() {
//            translate([mount_l, bounds[1]/2-4, -eps/2])
//            cylinder(d=ghole_d, h=base_th+eps);
//
//            translate([mount_l, -bounds[1]/2+4, -eps/2])
//            cylinder(d=ghole_d, h=base_th+eps);
//        }
//
//        hull() {
//            translate([mount_r, bounds[1]/2-4, -eps/2])
//            cylinder(d=ghole_d, h=base_th+eps);
//
//            translate([mount_r, -bounds[1]/2+4, -eps/2])
//            cylinder(d=ghole_d, h=base_th+eps);
//        }
//
//        cube(bounds, center=true);
//    }
}

//bias_t_support(clamp=false);
//rotate([0, 90, 0]) bias_t_support(clamp=true);
module bias_t_support(clamp=false) {
    tol = 0.25;

    bias_t_bounds = [63.54+tol, 44.44+tol, 28.77+tol];
    dim2 = 37.98;
    dim3 = 25.41;

    hole_h = 10.27;
    hole_d = 5;
    hole_sep = 12.75;

    lip = 5;
    lip_wall_h = 5;

    offset_h = con_center_h - bias_t_bounds[2]/2;

    rf_offset = 58.71 - 15.84/2;

    if(clamp) {
        clamp_h = bias_t_bounds[2] + wall_th - clamp_gap;

        render()
        //translate([-rf_offset+clamp_s/2, bias_t_bounds[1]/2, offset_h+clamp_gap])
        difference() {
            translate([0, 0, clamp_h/2])
            cube([clamp_s, bias_t_bounds[1]+(clamp_s+wall_th)*2, clamp_h], center=true);

            translate([0, 0, (clamp_h-wall_th)/2])
            cube([clamp_s, bias_t_bounds[1], clamp_h-wall_th], center=true);

            mirror_copy([0, 1, 0])
            translate([0, bias_t_bounds[1]/2+clamp_s, 0]) {
                translate([0, 0, clamp_h/2+wall_th])
                cube([clamp_s, clamp_s, clamp_h], center=true);

                cylinder(d=m3_d, h=clamp_h);
            }
        }
    }
    else {

    translate([-rf_offset, 0, 0]) {
        linear_extrude(offset_h)
        center_hole(lip)
        square([bias_t_bounds[0], bias_t_bounds[1]]);

        translate([bias_t_bounds[0], bias_t_bounds[1]/2 - dim3/2, 0])
        difference() {
            cube([wall_th, dim3, offset_h + 17]);

            translate([0, dim3/2, 0])
            mirror_copy([0, 1, 0])
            translate([-eps/2, hole_sep/2, hole_h+offset_h])
            rotate([0, 90, 0])
            cylinder(d=hole_d, h=wall_th+eps);
        }

        translate([0, bias_t_bounds[1]/2, 0])
        mirror_copy([0, 1, 0])
        translate([0, -bias_t_bounds[1]/2, 0])
        translate([0, -15, offset_h-5])
        render()
        difference() {
            cube([10, 15, 5]);

            translate([10/2, 10/2, 0])
            cylinder(d=m3_d, h=5);
        }
    }

    translate([0, bias_t_bounds[1]/2, 0]) {
        ghole_rails(1, bias_t_bounds[1]);

        mirror([1, 0, 0])
        ghole_rails(3, bias_t_bounds[1]);
    }
    }
}

module center_hole(l) {
    difference() {
        children();

        offset(-l)
        children();
    }
}

module vna_cable_support() {
}

//vna_connector_support(clamp=true);
//vna_connector_support(clamp=false);
module vna_connector_support(clamp=false, rails=true) {
    vna_connector_d = 21.8 + 1;
    vna_support_w = vna_connector_d + wall_th*2;
    rail_l = 25 + rail_edge_th*2;

    if(clamp) {
        render()
        difference() {
            linear_extrude(vna_support_l) {
                difference() {
                    translate([0, -clamp_gap])
                    difference() {
                        circle(d=vna_connector_d+wall_th*2);
                        circle(d=vna_connector_d);
                    }

                    translate([0, -100/2])
                    square(100, center=true);
                }

                mirror_copy([1, 0])
                translate([vna_support_w/2-wall_th, 0])
                square([10+wall_th, wall_th]);
            }

            mirror_copy([1, 0, 0])
            translate([vna_support_w/2 + 10/2, 0, vna_support_l/2])
            rotate([-90, 0, 0])
            cylinder(d=m3_clear_d, h=wall_th);
        }
    }
    else {
        render()
        difference() {
            union() {
                translate([0, -vna_support_l/2, 0])
                difference() {
                    translate([-vna_support_w/2, 0, 0])
                    cube([vna_support_w, vna_support_l, con_center_h]);

                    #translate([0, -eps/2, con_center_h])
                    rotate([-90, 0, 0])
                    cylinder(d=vna_connector_d, h=vna_support_l+eps);
                }

                mirror_copy([1, 0, 0])
                translate([vna_support_w/2, 0, con_center_h-wall_th*2])
                render()
                difference() {
                    translate([0, -vna_support_l/2, 0])
                    cube([10, vna_support_l, wall_th*2]);

                    translate([10/2, 0, 0])
                    cylinder(d=m3_clear_d, h=wall_th*2);
                }
            }

            translate([0, 0, con_center_h - 1/2])
            cube([vna_support_w+10*2, vna_support_l, 1], center=true);
        }

        ghole_n = 2;

        if(rails) {
            mirror_copy([1, 0])
            ghole_rails(1, rail_l);
        }
    }
}

module jig_base(board_dims, mhole_offset) {
    //jig_con_offset = 0.865;
    //jig_h = con_center_h - jig_con_offset;
    //echo(jig_h);
    jig_h = 49.135; // manually fixed
    echo(jig_h - z_offset);

    render()
    difference() {
        union() {
            linear_extrude(jig_h)
            difference() {
                square(board_dims, center=true);

                mirror_copy([0, 1])
                mirror_copy([1, 0])
                translate(mhole_offset)
                square(mhole_d, center=true);

                conn_ytra = 4;

                mirror_copy([1, 0])
                translate([-board_dims[0]/2, -(connector_bounds[1]+conn_ytra)/2])
                square([connector_bounds[0], connector_bounds[1]+conn_ytra]);
            }

            rotate(90)
            mirror_copy([1, 0])
            ghole_rails(1, rail_min_l);
        }

        mirror_copy([0, 1])
        mirror_copy([1, 0])
        translate(mhole_offset)
        union() {
            linear_extrude(jig_h-10)
            square(m3_head_d, center=true);

            translate([0, 0, jig_h-10])
            linear_extrude(0.5)
            square([m3_head_d, mhole_d], center=true);

            translate([0, 0, jig_h-10 + 0.5])
            linear_extrude(10)
            square(mhole_d, center=true);
        }
    }


    //rail_l = max(rail_min_l, bounds[1]);


//    linear_extrude(base_th)
//    difference() {
//        square(mount_dims, center=true);
//
//        mount_holes(mhole_offset);
//
//        mirror_copy([1, 0])
//        mirror_copy([0, 1])
//        for(ghole_x = [ghole_spacing/2 : 10 : mount_dims[0] - ghole_spacing/2]) {
//            for(ghole_y = [ghole_spacing/2 : 10 : mount_dims[0] - ghole_spacing/2]) {
//                if(ghole_x >= board_dims[0]/2 + 10 || ghole_y >= board_dims[1]/2 + 10) {
//                    translate([ghole_x, ghole_y]) circle(d = ghole_d);
//                }
//            }
//        }
//    }
}

module vert_cap_nut_hole(screw_l, screw_hole_d, nut_flat_hole, nut_th, nut_depth, nut_side_depth=10) {
    translate([0, 0, -screw_l])
    linear_extrude(screw_l)
    square(size=screw_hole_d, center=true);

    nut_inner_depth = (nut_flat_hole / (3^0.5/2))/2;
    translate([0, 0, -nut_th - nut_depth])
    linear_extrude(nut_th)
    translate([-nut_inner_depth, -nut_flat_hole/2])
    square([nut_side_depth + nut_inner_depth, nut_flat_hole]);

    translate([0, 0, -nut_depth])
    linear_extrude(0.5)
    square([screw_hole_d, nut_flat_hole], center=true);
}

module ghole_rails(n, rail_l, inner_pos=0, round_inner=false) {
    corner_d = ghole_d + rail_edge_th*2;
    corner_offset = (rail_l-corner_d)/2;

    rail_pos = ghole_spacing*n;

    linear_extrude(base_th)
    difference() {
        hull() {
            translate([rail_pos, 0])
            mirror_copy([0, 1])
            translate([0, corner_offset])
            circle(d=corner_d);

            if(round_inner) {
                translate([inner_pos+corner_d/2, 0])
                mirror_copy([0, 1])
                translate([0, corner_offset])
                circle(d=corner_d);
            }
            else {
                translate([inner_pos, -rail_l/2])
                square([eps, rail_l]);
            }
        }

        hull() {
            mirror_copy([0, 1])
            translate([rail_pos, corner_offset])
            circle(d=ghole_d);
        }
    }
}

module clamp(inner_w, inner_h) {
    render()
    difference() {
        translate([0, 0, (inner_h+wall_th)/2])
        cube([inner_w + (wall_th+10)*2, 10, inner_h + wall_th], center=true);

        translate([0, 0, inner_h/2])
        cube([inner_w, 10, inner_h], center=true);

        mirror_copy([1, 0, 0])
        translate([inner_w/2 + 10/2 + wall_th, 0, 0]) {
            translate([0, 0, inner_h/2 + wall_th])
            cube([10, 10, inner_h], center=true);

            cylinder(d=m3_clear_d, h=wall_th);
        }
    }
}

module mount_holes(mhole_offset) {
    mirror_copy([1, 0])
    mirror_copy([0, 1])
    translate(mhole_offset) circle(d = mhole_d);
}

module mirror_copy(v) {
    children();
    mirror(v) children();
}
